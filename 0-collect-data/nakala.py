# Récupérer les DOIs de l'UGA présents dans Nakala
## 2023-11-29, Maxence Larrieu

"""
# Particularités
- pas de données d'affiliations, on passe directement par les comptes des déposants qui nous ont été fournis par l'équipe HumaNum
- réponse de l'API dans la navigateur en XML et via requets en json

## Process
- charger les userid
- itérer sur tous les userid
- récupérer les DOIs de tous leurs dépôts
- exlure les actions de création de collection
- extra : recherche aussi de nouveau utilisateurs par l'intémédiaire des droits des dépôts effectués

## question sur le périmètre
- périmètre : est-ce qu'on inclut des personnels comme Thomas Leduc (univ. nantes) mais membre AAU ?
"""

def get_dois_n_users(user_id): 
    """
    attention logique d'utilisateurs (=compte nakala), pas d'auteur
    extrait les DOIs déposées par un user dans Nakala (les collections sont exclues)
    user_network : ressortir des userId avec qui les déposant ont partagés des droits de modif. sur leurs dépôts
    """
    
    buffer_dois = []
    other_users = []
    url_req = "https://api.nakala.fr/search?order=relevance&page=1&size=25&fq=depositor="
    req = requests.get(url_req + user_id)

    ## filtre si la requete n'aboutie pas
    if req.status_code != 200 : 
        return{
            "continue" : False,
            "content" : r,
            "other_users" : False
        }
        
    res = req.json()
    
    # iterer sur tous les résultats
    for item in res["datas"] : 
        ## exclure les collections, qui ne contiennes pas d'élement "files" 
        if not item.get("files") : 
            continue
            
        # récupérer le DOI
        buffer_dois.append( item.get("identifier") )

        # rechercher d'autres userId
        if item.get("rights") :
            ## pour toutes les personnes à qui on a ajouté des droits
            for person in item["rights"] :
                
                ## récupérer leurs userid
                person_userid = person.get("username")
                
                ## si on trouve un autre utilisateurs
                if person_userid and person_userid not in nakala_uga_users and person_userid not in other_users : 
                    other_users.append( person_userid )

    return{
        "continue" : True,
        "content" : buffer_dois,
        "other_users" : other_users
    }

import json, requests

print("\n\nRunning nakala.py")


## list to stock datas
nakala_uga_users = []
all_dois = []
other_user_finded = []

## load nakala users from txt file
with open('nakala-uga-users.txt', 'r') as f:
    ## attention bien stripper quand on import du txt sinon les sauts de ligne sont présents
    [nakala_uga_users.append( user.strip() ) for user in f.readlines()]

print("\tnb nakala users loaded", len(nakala_uga_users))

# ____n____ iterer sur les users uga
for user in nakala_uga_users : 

    ## rechercher les jeux de données
    res = get_dois_n_users(user)
    
    if res["continue"] :
        #print(f"{user}\n\tnb DOI {len(res['content'])}" )
        
        ## ajouter les DOIs trouvés
        if len(res["content"]) > 0 : 
            all_dois += res["content"]
        
        ## ajouter les personnes
        if len(res["other_users"]) >  0 : 
            #print(f"\nnew person {','.join(res['other_users'])}")
            other_user_finded += [x for x in res["other_users"] ]

print(f"\tnb dois finded\t\t{len(all_dois)}")


## ____n____ exporter les DOI au format txt

with open("nakala-dois.txt", 'w') as fh :
    [fh.write(f"{line}\n") for line in all_dois]

## print les autres utilisateurs trouvés
if other_user_finded : 
    print("\n\n\tnakala new user finded ")
    for elem in other_user_finded : 
        print(f"\t\t\t{elem}")

