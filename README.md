# Scripts & codes for the UGA Open research data monitor

See contextualized results on the website : [mlarrieu.gricad-pages.univ-grenoble-alpes.fr/open-research-data-monitor](https://mlarrieu.gricad-pages.univ-grenoble-alpes.fr/open-research-data-monitor)

<br />
<br />

## Sources & identification methods

### Recherche Data Gouv

- Recherche en format texte de `UGA` et `grenoble AND alpes` dans les champs suivants : `author`, `contributor`, `datasetContactAffiliation`, `producerAffiliation`

### DataCite

- recherche avec les clients Datacite de l'UGA : `inist.osug`, `client.uid:inist.sshade`, `client.uid:inist.resif`, `client_id:inist.persyval`

- avec les ROR de l'université sur les champs `creators` et `contributors`

- en format texte `grenoble AND alpes` sur le champs `publisher`

- instruire l'UGA comme financeur


### Zenodo

- recherche en format texte `"(\"grenoble alpes\" OR \"grenoble alps\" OR \"grenoble INP\" OR \"polytechnique de grenoble\" OR \"Grenoble Institute of Technology\" OR \"univeristé de grenoble\" )"` sur les champs `author` et `contributor`

- veille sur l'API car demain il devrait être possible de requêter par ROR ?

### Nakala

- recherche par les déposants relevant de l'UGA. Liste obtenues via HumaNum et enrichie manuellement

- instruire côté `dcterms:publisher`

### Barometre de la science ouverte UGA

- a faire annuellement à chaque MAJ du jeux de données
- récupérer la liste de publications, filter sur celles où des jeux de données ont été produits
- passer par HAL pour retrouver les DOI de ces jeux de données (champs `researchData_s`)

## Filters
- we remove the following datacite types `["Book", "ConferencePaper", "ConferenceProceeding", "JournalArticle", "BookChapter", "Service", "Preprint"]`
- we remove the following datacite clients `["rg.rg", "inist.epure"]`

## Comment sont comptées les données de la recherche ?

Le monitor prend en compte les données dotées d'un DOI de l'agence DataCite, c'est-à-dire qu'elles sont Findable. Un dépôt de données comprend des métadonnées conformes au schéma de données DataCite et un ou plusieurs fichiers pouvant être organisés en arborescence. Ce sont les dépôts qui sont comptés et non les fichiers intégrés aux dépôts : un DOI compte donc pour une donnée de recherche.
Le schéma de Datacite permet de déclarer des relations entre DOI, ce que nous utilisons pour gérer les versions ou les doublons de données.
Afin d'éviter de compter deux pour un même dépôt, ou bien pour un dépôt mis à jour, le monitor est doté d'une fonction qui navigue entre les DOIs dont la relation est de type  `isVersionOf` ou `isIdenticalTo`. Dans le premier cas, la fonction "remonte" les versions jusqu'à la version parente, c'est-à-dire un DOI stable qui redirige vers la version la plus récente.
Dans le deuxième cas, la fonction garde simplement la version signalée comme étant identique. Cette relation n'étant pas symétrique, le DOI conservé n'aura pas de relation "isIdenticalTo" et la redondance est évitée.


<br />
<br />


## Data schema
Les champs du tableau produit reprennent ceux du schéma de données de DataCite (cf. https://datacite-metadata-schema.readthedocs.io/en/4.5/), auquel deux champs sont ajoutés :
- `all_relation`
toutes les relations attachées au DOI identifié.

- `traveled_dois`
liste des DOIs parcourus par le script pour obtenir le DOI de concept


<br />
<br />


## Credits

* Élias Chetouane: collecting data, program automation
* Maxence Larrieu: collecting data, enrichment & visualisation

as members of GRICAD & CDGA
