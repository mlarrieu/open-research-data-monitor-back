import subprocess


"""
0. collect DOIs
1. concatenate & enrich with dataCite API
2. produce graphs
"""

def run_py_file(file_name, parent_folder):
	"""
	excute a py program
	"""
	try :
		## cwd py programs path
		subprocess.run(['python', file_name], cwd= parent_folder)
	except FileNotFoundError:
		print(f"error: the file '{file_name}' does not exist")


# ______0______ collect DOIs
collec_dois_prgm = [
	"zenodo.py",
	"nakala.py",
	"datacite.py",
	"rdg.py"
]


#to depug : run only one script	
collec_dois_prgm = ["rdg.py", "datacite.py", "nakala.py", "zenodo.py"] 
[run_py_file(file, "0-collect-data") for file in collec_dois_prgm]




## ______1______ Concatenate and enrich DOIs w DataCite
run_py_file("concatenate-enrich-dois.py", "1-enrich-with-datacite")



# ______2______ Produce graphics

produce_graph_prgm = [
	"hist-evol-datasets-per-repo.py",
	"hist-quantity-year-type.py",
	"pie-datacite-client.py",
	"pie-data-type.py",
	"hist-last-datasets-by-client.py"
]

[run_py_file(file, "2-produce-graph") for file in produce_graph_prgm]




