import pandas as pd


def load_and_treat_csv() :
	
	df_raw = pd.read_csv("../dois-uga.csv", index_col=False)
	
	## remove datacite type that are not "research data"
	type_to_explude = ["Book", "ConferencePaper", "ConferenceProceeding", "JournalArticle", "BookChapter", "Service", "Preprint"]
	df = df_raw[ ~df_raw["resourceTypeGeneral"].isin(type_to_explude) ].copy()
	
	return df

	