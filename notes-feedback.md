

2024-03-14 presentation Matrice
=================

Journées Mathrice à Grenoble, les 13 et 14 Mars 2024
https://indico.math.cnrs.fr/event/10998/page/779-journees-mathrice-a-grenoble-les-13-et-14-mars-2024

## Trois questions

- que se passe-t-il en cas de doubon de DOI ? 
- Y a t il une variation disciplinaire par rapport aux formats (xml, csv)
- nous faut il répliquer nos DOI dans Recherche Data Gouv pour plus de visibilité ? 


